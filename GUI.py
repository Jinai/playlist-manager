# -*- coding: utf-8 -*-
# !python3

"""
Aaaah Playlist Manager (2015)
Générateur de playlists aléatoires avec paramètrage sur la sélection
des maps par rapport à leur niveau
"""

import os
import sys
import subprocess
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as mbox
import tkinter.filedialog as fdialog

import pebble
import pyperclip
import requests
import requests.exceptions

import tipsdata
import icondata
import playlist
import config
import update
from widgets import mapchecker, form, info, logger, treelist, ratiocontroller, tooltip, iconutils
from _version import __version__, __date__

__author__ = "Jinai"
__contributor__ = "Pwneyd"
__handyman__ = "Garganta"



def init():
    subprocess.call(["ini.exe", __version__], shell=True)
    sys.stderr = open(os.path.join(config.app_dir, "error.log"), "a")
    return config.get_config()


class PlaylistManager(tk.Tk):
    def __init__(self, conf, master=None):
        tk.Tk.__init__(self, master)
        self.master = master
        self.conf = conf
        self.playlist = playlist.Playlist()
        self.tooltips = []
        self._setup_widgets()

        # UI settings
        self.log("Playlist Manager v{} ({})".format(__version__, __date__), tag=None, timestamp=True, separator=True)
        try:
            self.tk.call('encoding', 'system', 'utf-8')
            icon = tk.PhotoImage(data=icondata.icon_aaaah)
            self.tk.call('wm', 'iconphoto', self._w, icon)
        except:
            pass
        self.title("Aaaah Playlist Manager v" + __version__)
        resizeable = self.conf.getboolean('UI', 'resizeable', fallback=False)
        self.resizable(width=resizeable, height=resizeable)
        self.update_idletasks()
        self.minsize(self.winfo_reqwidth(), self.winfo_reqheight())
        if not self.conf.getboolean('UI', 'show_tooltips', fallback=True):
            self.toggle_tooltips(silent=True)

        # Load last used playlist
        # try:
        #     self.playlist.open(self.conf['DIRECTORIES']['last_playlist'])
        # except IOError:
        #     if self.conf['DIRECTORIES']['last_playlist']:
        #         self.log("Erreur pendant le chargement de la dernière playlist {}".format(
        #             self.conf['DIRECTORIES']['last_playlist']),
        #                  tag="failure")
        # except KeyError:
        #     pass
        # else:
        #     self.refresh()

        self.import_master_playlist()
        self.check_update()

    def _setup_widgets(self):
        main_frame = tk.Frame(self)
        main_frame.pack(fill='both', expand=True, pady=5, padx=5)

        # -------------------------------------------------- MENU -------------------------------------------------- #

        self.option_add('*tearOff', False)  # Prevents ugly, old detachable menu
        menubar = tk.Menu(self)
        menubar.add_command(label="À propos", command=self.about)
        menubar.add_command(label="Aide", command=self.help)
        menubar.add_command(label="Tooltips on/off", command=self.toggle_tooltips)
        menubar.add_command(label="Vérifier MÀJ", command=self.check_update)
        self.config(menu=menubar)

        self.bind('<Control-s>', lambda _: self.export())
        self.bind('<Control-o>', lambda _: self.import_())
        self.bind('<Control-q>', lambda _: self.quit())
        self.bind('<Control-h>', lambda _: self.help())

        # ------------------------------------------------- LOGGER ------------------------------------------------- #

        self.logger = logger.Logger(main_frame, width=60, height=10, wrap="word", enable_buttons=False)

        # ------------------------------------------ NOTEBOOK (MAPLISTS) ------------------------------------------- #

        self.frame_notebook = tk.Frame(main_frame)
        self.notebook = ttk.Notebook(self.frame_notebook)
        self.notebook.pack(fill='both', expand=True)
        self.sizegrip = ttk.Sizegrip(self.frame_notebook)
        self.sizegrip.pack(anchor='se')

        self.tree_all = treelist.Maplist(self.notebook, ('code', 'auteur', 'titre', 'level', 'def lvl', 'commentaire'),
                                         self.playlist, 'all', self.logger, column_ignore=(1, 5), search_ignore=(5, 6),
                                         refresh_cmd=self.refresh)
        self.tree_all.pack(fill='both', expand=True)
        self.tree_easy = treelist.Maplist(self.notebook, ('code', 'auteur', 'titre', 'level', 'def lvl', 'commentaire'),
                                          self.playlist, 'easy', self.logger, refresh_cmd=lambda: self.info.update())
        self.tree_easy.pack(fill='both', expand=True)
        self.tree_medium = treelist.Maplist(self.notebook, ('code', 'auteur', 'titre', 'level', 'def lvl', 'commentaire'),
                                            self.playlist, 'medium', self.logger, refresh_cmd=lambda: self.info.update())
        self.tree_medium.pack(fill='both', expand=True)
        self.tree_hard = treelist.Maplist(self.notebook, ('code', 'auteur', 'titre', 'level', 'def lvl', 'commentaire'),
                                          self.playlist, 'hard', self.logger, refresh_cmd=lambda: self.info.update())
        self.tree_hard.pack(fill='both', expand=True)

        self.notebook.add(self.tree_all, text="   All   ", padding=1)
        self.notebook.add(self.tree_easy, text="   Easy   ", padding=1)
        self.notebook.add(self.tree_medium, text="   Medium   ", padding=1)
        self.notebook.add(self.tree_hard, text="   Hard   ", padding=1)

        # -------------------------------------------------- INFO -------------------------------------------------- #

        title = iconutils.IconTitle(icondata.icon_info, "Info", padx=2)
        self.info = info.Info(main_frame, ('Maps easy:', 'Maps medium :', 'Maps hard :', 'Total :'), self.playlist,
                              labelwidget=title)

        # -------------------------------------------------- FORM -------------------------------------------------- #

        title = iconutils.IconTitle(icondata.icon_plus, "Ajouter map", padx=2)
        self.form = form.Form(main_frame, ('Code : ', 'Titre : ', 'Auteur : ', 'Commentaire : '), self.playlist,
                              self.logger, labelwidget=title, refresh_cmd=self.refresh)

        # ----------------------------------------------- MAPCHECKER ----------------------------------------------- #

        title = iconutils.IconTitle(icondata.icon_file_valid, "Map checker", padx=2)
        self.mapchecker = mapchecker.MapChecker(main_frame, playlist=self.playlist, logger=self.logger, form=self.form,
                                                refresh_cmd=self.refresh, labelwidget=title)

        # -------------------------------------------- RATIOCONTROLLER --------------------------------------------- #

        title = iconutils.IconTitle(icondata.icon_gear, "Paramètres", padx=2)
        self.ratiocontroller = ratiocontroller.RatioController(main_frame, ('Easy :', 'Medium :', 'Hard :'),
                                                               self.playlist, (25, 50, 25), labelwidget=title)

        # -------------------------------------------- IMPORT / EXPORT --------------------------------------------- #

        self.frame_impexp = tk.Frame(main_frame)
        button_import = ttk.Button(self.frame_impexp, text="Importer", command=self.import_)
        button_import.pack(side='left')
        button_export = ttk.Button(self.frame_impexp, text="Exporter", command=self.export)
        button_export.pack(side='left')

        # ----------------------------------------------- GENERATION ----------------------------------------------- #

        ttk.Style().configure('Gen.TButton', background='chartreuse3')
        self.button_generate = ttk.Button(main_frame, text="Générer playlist", style="Gen.TButton",
                                          command=self.generate_playlist)

        # ------------------------------------------------ TOOLTIPS ------------------------------------------------ #

        for maplist in (self.tree_all, self.tree_easy, self.tree_medium, self.tree_hard):
            self.tooltips.append(tooltip.DynamicToolTip(maplist.entry_search, tipsdata.maplist['search']))
            self.tooltips.append(tooltip.DynamicToolTip(maplist.button_delete, tipsdata.maplist['delete']))
            self.tooltips.append(tooltip.DynamicToolTip(maplist.button_quickpl, tipsdata.maplist['quickplaylist']))
        self.tooltips.append(tooltip.DynamicToolTip(self.mapchecker.button_check, tipsdata.mapchecker['check']))
        self.tooltips.append(tooltip.DynamicToolTip(self.mapchecker.button_compare, tipsdata.mapchecker['comp']))
        self.tooltips.append(tooltip.DynamicToolTip(self.mapchecker.button_download, tipsdata.mapchecker['download']))
        self.tooltips.append(tooltip.DynamicToolTip(self.mapchecker.button_refresh, tipsdata.mapchecker['refresh']))
        self.tooltips.append(tooltip.DynamicToolTip(self.ratiocontroller.cb_random, tipsdata.ratiocontroller['random']))
        self.tooltips.append(tooltip.DynamicToolTip(self.ratiocontroller.entry_amount, tipsdata.ratiocontroller['total']))
        self.tooltips.append(tooltip.DynamicToolTip(self.ratiocontroller.config0, tipsdata.ratiocontroller['@']))
        self.tooltips.append(tooltip.DynamicToolTip(self.ratiocontroller.button_reset, tipsdata.ratiocontroller['reset']))

        # ------------------------------------------- WIDGETS PLACEMENT -------------------------------------------- #

        self.frame_notebook.grid(row=0, column=0, rowspan=3, padx=(0, 5), sticky='nsew')
        self.mapchecker.grid(row=0, column=1, padx=(0, 5), sticky='nsew')
        self.info.grid(row=0, column=2, sticky='nsew')
        self.form.grid(row=1, column=1, pady=5, padx=(0, 5), sticky='nsew')
        self.ratiocontroller.grid(row=1, column=2, pady=5, sticky='nsew')
        self.logger.grid(row=2, column=1, columnspan=2, sticky='nsew')
        self.frame_impexp.grid(row=3, column=0, pady=(5, 0))
        self.button_generate.grid(row=3, column=1, columnspan=2, pady=(5, 0))

        main_frame.grid_rowconfigure(2, weight=1)
        main_frame.grid_columnconfigure(0, weight=10)
        main_frame.grid_columnconfigure((1, 2), weight=1)

    def generate_playlist(self):
        e, m, h = self.ratiocontroller.get()
        t, r = self.ratiocontroller.amount(), self.ratiocontroller.random()
        playlist = self.playlist.generate_playlist(t, e / 100, m / 100, h / 100, r)
        size = len(playlist)
        if size < t:
            self.log("Pas assez de maps. {} maps copiée(s) dans le presse-papiers".format(size), tag='warning')
        else:
            self.log("{} maps copiée(s) dans le presse-papiers".format(t))
        pyperclip.copy(str(playlist))
        file_name = fdialog.asksaveasfilename(initialdir=self.conf.get('DIRECTORIES','generate_playlist', fallback=""),
                                              initialfile='playlist_'+str(t), defaultextension='.txt')
        if file_name:
            try:
                f = open(file_name, 'w', encoding='utf-8')
                f.write(str(playlist))
            except IOError as e:
                self.log(str(e), tag='failure')
            else:
                f.close()
                self.log("Playlist exportée : {}".format(file_name))
            self.edit_conf("DIRECTORIES", ("generate_playlist", os.path.dirname(file_name)))

    def refresh(self):
        self.info.playlist = self.playlist
        self.mapchecker.playlist = self.playlist
        self.ratiocontroller.playlist = self.playlist
        self.tree_all.playlist = self.playlist
        self.tree_easy.playlist = self.playlist
        self.tree_medium.playlist = self.playlist
        self.tree_hard.playlist = self.playlist
        self.info.update()
        self.tree_all.refresh()
        self.tree_easy.refresh()
        self.tree_medium.refresh()
        self.tree_hard.refresh()

    @pebble.thread.concurrent
    def import_master_playlist(self):
        try:
            data = requests.get(mapchecker.URL_PLAYLIST, headers=mapchecker.HEADERS, timeout=mapchecker.TIMEOUT)
        except requests.exceptions.RequestException as e:
            self.log(str(e), tag="failure")
        else:
            p = playlist.Playlist()
            p.from_string(data.text)
            self.playlist = p
            self.refresh()

    def import_(self):
        file_name = fdialog.askopenfilename(initialdir=self.conf.get("DIRECTORIES", "import_playlist", fallback=""),
                                            filetypes=(("Text Files", "*.txt"), ("All Files", "*.*")))
        if file_name:
            p = playlist.Playlist()
            try:
                p.open(file_name)
            except (IOError, ValueError) as e:
                self.log(str(e), tag='failure')
            except:
                self.log(sys.exc_info()[1], tag='failure')
            else:
                self.playlist = p
                self.refresh()
                self.log("Playlist importée : {}".format(repr(p)[1:-1]), tag='success')
                self.edit_conf("DIRECTORIES", ("import_playlist", os.path.dirname(file_name)))
                self.edit_conf("DIRECTORIES", ("last_playlist", file_name))

    def export(self):
        file_name = fdialog.asksaveasfilename(initialdir=self.conf.get("DIRECTORIES", "export_playlist", fallback=""),
                                              initialfile='playlist', defaultextension='.txt')
        if file_name:
            try:
                f = open(file_name, 'w', encoding='utf-8')
                f.write(str(self.playlist))
            except IOError as e:
                self.log(str(e), tag='failure')
            else:
                f.close()
                self.log("Playlist exportée : {}".format(file_name))
            self.edit_conf('DIRECTORIES', ('export_playlist', os.path.dirname(file_name)))

    def toggle_tooltips(self, silent=False):
        for tooltip in self.tooltips:
            tooltip.toggle()
        visible = self.tooltips[0].visible
        self.edit_conf('UI', ('show_tooltips', str(visible)))
        if not silent:
            self.log("Tooltips: " + {1:"On", 0:"Off"}[visible])

    def check_update(self, task=None):
        if not task:
            pebble.thread.concurrent(target=update.available, callback=self.check_update)
        else:
            if task.get():
                subprocess.Popen(["updater.exe"], shell=True)
            else:
                self.log("Vous êtes à jour !", tag="success")

    def edit_conf(self, section, keyval):
        config.edit_config(self.conf, section, keyval)

    def help(self):
        mbox.showinfo("Aide", "http://playlist-manager.olympe.in/manager/aide.txt\n\nSi vous avez "
                              "un problème ou une question, n'hésitez pas à nous contacter sur le forum, par chucho "
                              "ou par Skype.\nJinai : jinaioo\nGarganta : gargantouille")

    def about(self):
        mbox.showinfo("À propos",
                      "Aaaah Playlist Manager v{}\nCodé en Python avec Tkinter\n\nChef de projet : {}"
                      "\nAssistant : {}\nHomme à tout faire : {}".format(__version__, __author__, __contributor__,
                                                                         __handyman__))

    def log(self, event, tag=None, timestamp=True, separator=False):
        if separator:
            self.logger.insert("- " * 10, timestamp=False)
            self.logger.insert(event, tag, timestamp)
            self.logger.insert("- " * 10, timestamp=False)
        else:
            self.logger.insert(event, tag, timestamp)

    def quit(self):
        raise SystemExit


if __name__ == "__main__":
    pl_manager = PlaylistManager(init())
    pl_manager.mainloop()
