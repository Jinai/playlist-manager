# -*- coding: utf-8 -*-
# !python3

import os
import tempfile
import configparser

import requests
import requests.exceptions

path_ini = os.path.join(os.path.normpath(os.path.join(os.path.expanduser("~"), "Documents", "Playlist Manager")),
                        "config.ini")
link_version = "http://playlist-manager.olympe.in/update/version.txt"
link_new = "http://playlist-manager.olympe.in/update/whatisnew.txt"
link_executable = "http://playlist-manager.olympe.in/manager/download.php"
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0'}
website = "http://playlist-manager.olympe.in/"
icon_aaaah = """
    iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAuUlEQVQ4jZWTYRGD
    MAxGHzMCDiqBOdgcIGESJgEJlbA5mAQklCnoHLA/6TUrLV2/u1xzJV/oo6Ejrx4Y
    JF+Bd6EuqxHYkhhbGjgxLSp3gJV80sVdpsEm6wB8JLSusrdSQPPSxAL3BMWp566E
    MLP/Bmm4FCWHkTPNgDkywu8teEG51ExagfvxT/Eps3eW9dny1qCJePwqa6owKCFu
    LWajjC8q91zSohqEYWn6B4wYPXGYbOspgnoi0qG+Iw9LoQcUz5IAAAAASUVORK5C
    YII=
"""


def local_version():
    conf = configparser.ConfigParser()
    conf.read(path_ini)
    try:
        local_ver = conf["UPDATE"]["this_version"]
    except KeyError:
        local_ver = "Unknown"
    return local_ver


def latest_version():
    try:
        latest_ver = requests.get(link_version, headers=headers, timeout=5).text
    except requests.exceptions.RequestException:
        latest_ver = "Unknown"
    return latest_ver


def whatisnew():
    try:
        new = requests.get(link_new, headers=headers, timeout=5).text
    except requests.exceptions.RequestException:
        new = None
    return new


def executable_path():
    try:
        r = requests.get(link_executable, headers=headers, stream=True)
    except requests.exceptions.RequestException:
        return None
    else:
        with tempfile.NamedTemporaryFile(prefix="playlistmanagerupdate", suffix=".exe", delete=False) as f:
            for chunk in r.iter_content(chunk_size=2 ** 16):
                f.write(chunk)
                f.flush()
                os.fsync(f.fileno())
        return f.name
