# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as mbox
from webbrowser import open
from subprocess import Popen

from pebble import thread

import utils


class UpdateWindow(tk.Tk):
    def __init__(self, latest_version, master=None, **opts):
        tk.Tk.__init__(self, master)
        self.master = master
        self.latest_version = latest_version

        self.title("Mise à jour")
        icon = tk.PhotoImage(data=utils.icon_aaaah)
        self.tk.call('wm', 'iconphoto', self._w, icon)
        self._setup_widgets()
        self.attributes("-topmost", True)
        self.attributes("-topmost", False)
        self.wm_overrideredirect(0)
        self.grab_set()

    def _setup_widgets(self):
        s = ttk.Style()
        s.configure("Title.TLabel", foreground="gray25", font="impact 20")
        s.configure("Version.TLabel", foreground="gray20", font="verdana 10")
        self.withdraw()  # Hides window before rendering widgets
        self.main_frame = tk.Frame(self, bd=1, relief="solid", padx=5, pady=5)
        self.main_frame.pack(fill="both", expand=True)
        self.label_maj = ttk.Label(self.main_frame, text="Une mise à jour est disponible", style="Title.TLabel")
        self.label_maj.pack(pady=(0, 5))
        self.label_version = ttk.Label(self.main_frame, text="Version actuelle : " + utils.local_version(),
                                       style="Version.TLabel")
        self.label_version.pack(fill="x")
        self.frame_scrolledtext = ttk.Frame(self.main_frame)
        self.frame_scrolledtext.pack(fill="both", expand=True)
        self.textbox = tk.Text(self.frame_scrolledtext, width=75, height=10)
        self.textbox.pack(side="left", fill="both", expand=True)
        self.textbox.config(state="disabled")
        self.textbox.config(font="calibri 8", borderwidth=1, relief="sunken", bg="gray80", wrap="word")
        self.scrollbar = ttk.Scrollbar(self.frame_scrolledtext)
        self.scrollbar.pack(side="right", fill="y")
        self.scrollbar.config(command=self.textbox.yview)
        self.textbox.config(yscrollcommand=self.scrollbar.set)
        self.frame_yesno = ttk.Frame(self.main_frame)
        self.frame_yesno.pack(pady=(5, 0))
        self.button_yes = ttk.Button(self.frame_yesno, text="Télécharger", command=self.yes)
        self.button_yes.pack(side="left")
        self.button_no = ttk.Button(self.frame_yesno, text="Pas maintenant", command=self.no)
        self.button_no.pack(side="right")
        self.center()
        self.deiconify()  # Show the window once everything is good

    def center(self):
        self.update_idletasks()
        self.geometry("+%d+%d" % ((self.winfo_screenwidth() / 2) - (self.winfo_reqwidth() / 2),
                                  (self.winfo_screenheight() / 2) - (self.winfo_reqheight() / 2)))

    def get_changelog(self):
        thread.concurrent(target=utils.whatisnew, callback=self.fill)

    def fill(self, task):
        content = task.get()
        self.textbox.config(state="normal")
        if content:
            self.textbox.insert("end", content)
        else:
            self.textbox.insert("end", "Erreur pendant le chargement du changelog")
        self.textbox.config(state="disabled")

    def yes(self):
        DownloadWindow(self, utils.website, self.latest_version).download()
        self.withdraw()

    def no(self):
        self.destroy()


class DownloadWindow(tk.Toplevel):
    def __init__(self, master, dl_link, update_version, interval=10, openf=True, **opts):
        tk.Toplevel.__init__(self, master, **opts)
        self.master = master
        self.dl_link = dl_link
        self.update_version = update_version
        self.interval = interval
        self.openf = openf
        self._setup_widgets()
        self.attributes("-topmost", True)
        self.attributes("-topmost", False)
        self.wm_overrideredirect(1)
        self.grab_set()

    def _setup_widgets(self):
        self.withdraw()
        self.main_frame = tk.Frame(self, bd=1, relief="solid", padx=5, pady=5)
        self.main_frame.pack(fill="both", expand=True)
        self.label_download = ttk.Label(self.main_frame, text="Downloading v" + self.update_version)
        self.label_download.pack(pady=(0, 5))
        self.progressbar = ttk.Progressbar(self.main_frame, orient="horizontal", length=200, mode="indeterminate")
        self.progressbar.pack(fill="x")
        self.progressbar.start(interval=self.interval)
        self.center()
        self.deiconify()

    def center(self):
        self.update_idletasks()
        self.geometry(
            "+%d+%d" % ((self.master.winfo_rootx() + ((self.master.winfo_width() / 2) - (self.winfo_reqwidth() / 2)),
                         self.master.winfo_rooty() + ((self.master.winfo_height() / 2) - (self.winfo_reqheight() / 2))))
        )

    def download(self):
        thread.concurrent(target=utils.executable_path, callback=self.run)

    def run(self, task):
        if self.openf:
            path = task.get()
            if path:
                Popen(path, shell=True)
            elif mbox.askyesno("Erreur", "Une erreur s'est produite durant la mise à jour."
                                         "\nSouhaitez-vous aller sur le site pour télécharger la mise à jour ?"):
                open(utils.website)
        self.master.destroy()


if __name__ == "__main__":
    update = UpdateWindow(utils.latest_version())
    update.get_changelog()
    update.mainloop()
