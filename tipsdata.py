# -*- coding: utf-8 -*-
# !python3

mapchecker = {
    "all": "/!\ Requiert une connexion internet. /!\\",
    "check": "Regarde si le code proposé est déjà dans la playlist centrale."
             "\nDoublon/erreur = croix rouge",
    "comp": "Compare votre playlist à la playlist centrale."
            "\nS'il y a des doublons vous aurez la possibilité de les supprimer.",
    "download": "Permet de télécharger la playlist centrale sur votre ordinateur."
                "\nAccessoirement cela rafraîchit le cache.",
    "refresh": "La playlist centrale n'est téléchargée qu'une seule fois puis est"
              "\nstockée temporairement pour accélérer les comparaisons."
              "\nCette option permet de rafraîchir cette copie.",
}

ratiocontroller = {
    "all": "Permet le contrôle du ratio de maps par level.",
    "random": "Si Random est coché, les ratios ne seront pas pris en compte et"
              "\nla playlist sera totalement aléatoire. À décocher pour utiliser les ratios.",
    "total": "Le nombre de maps générées, 200 est suffisant pour un match."
             "\nLe jeu limite les playlists à 500 mapsdonc mettre plus ne sert à rien.",
    "@": "Permet d'avoir le ratio actuel de la playlist, les 4 autres sont des"
         "\nconfigs préfaites (pour le moment à titre d'exemple).",
    "reset": "Remet tous les paramètres par défaut.",
}

maplist = {
    "search": "Permet la recherche d'un mot-clé dans la liste (@code aussi).",
    "delete": "Permet de supprimer les maps sélectionnées de la playlist",
    "quickplaylist": "Copie dans votre presse-papiers les maps sélectionnées sous forme d'une"
                     "\nplaylist. À coller dans un fichier texte pour load in game.",
}
