# -*- coding: utf-8 -*-
# !python3

import sys


def py2exe():
    from distutils.core import setup
    import py2exe

    dist_dir = 'dist'

    setup(
        name="Playlist Manager",
        version="1.1.0",
        description="A playlist manager for the Playlist Rally project",
        author="Jinai",
        author_email="jinai.extinction@gmail.com",
        url="http://playlist-manager.olympe.in/",
        options={"py2exe": {"compressed": True,
                            "optimize": 2,
                            "bundle_files": 2,
                            "dist_dir": dist_dir,
                            "dll_excludes": ['msvcr71.dll', 'pywintypes34.dll'],
                            "excludes": ['doctest', 'unittest', 'xml', 'xmlrpc', 'difflib',
                                         'optparse', 'dis', 'bz2', 'bdb', 'ftplib', 'optparse', 'pdb', 'pydoc',
                                         'pyexpat', 'pywintypes', 'selectors', 'socketserver', 'win32api', 'win32con',
                                         '_bz2', '_hashlib', '_lzma', '_ssl', 'argparse', 'gettext', 'netbios', 'netrc',
                                         'pkgutil', 'plistlib', 'pprint', 'py_compile', 'runpy', 'ssl',
                                         'win32wnet', 'zipfile', '_multiprocessing', '_osx_support',
                                         '_strptime', '_threading_local', 'lzma', 'gzip', 'glob', 'getopt', 'getpass',
                                         'hmac', 'webbrowser',],
                            }
                 },
        zipfile=None,
        windows=[{"script": "GUI.py",
                  "icon_resources": [(0, "resources/aaaah.ico")],
                  "dest_base": "playlist_manager",
                  }],
        data_files=[('resources', ['resources/aaaah.ico']),
                    ('', ['build.iss']),
                    ('', ['ini/dist/ini.exe']),
                    ('', ['updater/dist/updater.exe'])]
    )


if __name__ == '__main__':
    py2exe()