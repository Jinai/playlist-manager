# -*- coding: utf-8 -*-
# !python3

import os
import configparser

# TODO :
#   [UI]
#       res.X
#       res.Y
#       columnSpace


app_name = "Playlist Manager"
config_name = "config.ini"
app_dir = os.path.normpath(os.path.join(os.path.expanduser("~"), "Documents", app_name))
config_path = os.path.join(app_dir, config_name)


def get_config():
    config = configparser.ConfigParser()
    config.read(config_path)
    return config


def edit_config(conf, section, keyval):
    try:
        conf.add_section(section)
    except configparser.DuplicateSectionError:
        pass
    conf.set(section, keyval[0], keyval[1])
    write(conf)


def write(conf):
    try:
        f = open(os.path.join(app_dir, config_name), "w")
        conf.write(f)
    except IOError:
        pass
    else:
        f.close()
