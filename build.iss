[Setup]
AppName=Playlist Manager
AppVersion=1.1.2
AppPublisher=Jinai
AppPublisherURL=http://playlist-manager.olympe.in/
DefaultDirName={pf}\Playlist Manager
DisableProgramGroupPage=yes
UninstallDisplayIcon={app}\playlist_manager.exe
OutputDir=C:\Users\Jinai\Desktop\
OutputBaseFilename=playlist_manager_v1.1.2
VersionInfoProductTextVersion=1.1.2
VersionInfoProductVersion=1.1.2
VersionInfoDescription=Outil de gestion de playlists pour le projet Playlist Rally
VersionInfoVersion=1.1.2

[Files]
Source: "*"; Excludes: "build.iss"; DestDir: "{app}"
Source: "tcl\*"; DestDir: "{app}\tcl"; Flags: recursesubdirs
Source: "resources\*"; DestDir: "{app}\resources"; Flags: recursesubdirs

[Icons]
Name: "{commondesktop}\Playlist Manager"; Filename: "{app}\playlist_manager.exe"; IconFilename: "{app}\resources\aaaah.ico"

[Run]
Filename: {app}\playlist_manager.exe; Description: Run Playlist Manager; Flags: postinstall nowait skipifsilent