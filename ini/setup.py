# -*- coding: utf-8 -*-
# !python3

import sys


def py2exe():
    from distutils.core import setup
    import py2exe

    dist_dir = 'dist'

    setup(
        options={"py2exe": {"compressed": True,
                            "optimize": 2,
                            "bundle_files": 2,
                            "dist_dir": dist_dir,
                            "dll_excludes": ['msvcr71.dll', 'pywintypes34.dll'],
                            "excludes": ['doctest', 'unittest', 'xml', 'xmlrpc', 'difflib',
                                         'optparse', 'dis', 'bz2', 'bdb', 'ftplib', 'optparse', 'pdb', 'pydoc',
                                         'pyexpat', 'pywintypes', 'selectors', 'socketserver', 'win32api', 'win32con',
                                         '_bz2', '_hashlib', '_lzma', '_ssl', 'argparse', 'gettext', 'netbios', 'netrc',
                                         'pkgutil', 'plistlib', 'pprint', 'py_compile', 'runpy', 'ssl', 'textwrap',
                                         'win32wnet', 'zipfile', '_multiprocessing', '_osx_support',
                                         '_strptime', '_threading_local', 'lzma', 'gzip', 'glob', 'getopt', 'getpass',
                                         'hmac', 'multiprocessing', 'inspect', 'pickle', 'shlex', 'webbrowser',
                                         'stringprep', 'subprocess', 'threading', 'unicodedata', 'base64', 'copy',
                                         '_ctypes', '_dummy_thread', 'posixpath', 'quopri'],
                            }
                 },
        zipfile=None,
        windows=[{"script": "ini.py"}],
    )


if __name__ == '__main__':
    py2exe()