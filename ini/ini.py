# -*- coding: utf-8 -*-
# !python3

import os
import configparser

__version__ = "1.1.1"

app_name = "Playlist Manager"
config_name = "config.ini"
app_dir = os.path.normpath(os.path.join(os.path.expanduser("~"), "Documents", app_name))
config_path = os.path.join(app_dir, config_name)

import_playlist = os.path.normpath(os.path.join(os.path.expanduser("~"), "Desktop"))
export_playlist = os.path.normpath(os.path.join(os.path.expanduser("~"), "Desktop"))
generate_playlist = os.path.normpath(os.path.join(os.path.expanduser("~"), "Desktop"))
last_playlist = ''

default_config = """
    [DIRECTORIES]
    application = {}
    logs = {}
    mapchecker = {}
    import_playlist = {}
    export_playlist = {}
    generate_playlist = {}
    last_playlist = {}

    [UI]
    resizeable = True
    show_tooltips = True

    [GENERATION]
    random = True
    total = 200
    last_ratio =
    config1 = 10,70,20
    config2 = 33,34,33
    config3 = 40,40,20
    config4 = 20,40,40

    [LIST]
    # ligne = 0, code = 1, auteur = 2, titre = 3, level = 4, level def = 5, commentaire = 6
    column_ignore_all = 1,5
    search_ignore_all = 5,6
    column_ignore = 1,4,5
    search_ignore = 4,5,6

    [PLAYLIST_URL]
    master_playlist = http://playlist-manager.olympe.in/playlists/master_playlist.txt
    maps_nc = http://playlist-manager.olympe.in/playlists/maps_nc.txt

    [UPDATE]
    this_version = {}
    latest_version = http://playlist-manager.olympe.in/update/version.txt
    whatisnew = http://playlist-manager.olympe.in/update/whatisnew.txt
""" \
    .format(app_dir, app_dir, app_dir, import_playlist, export_playlist, generate_playlist, last_playlist, __version__)


def create_env():
    OVERRIDE_VERSION = True
    OVERRIDE_IGNORE = True
    try:
        os.makedirs(app_dir)
    except OSError:
        pass  # Directory already exists
    if not os.path.isfile(config_path):  # .ini doesn't exist yet
        config = configparser.ConfigParser(allow_no_value=True)
        config.read_string(default_config)
        write(config)
    if OVERRIDE_VERSION:
        config = configparser.ConfigParser(allow_no_value=True)
        config.read(config_path)
        try:
            config.add_section("UPDATE")
        except configparser.DuplicateSectionError:
            pass
        config.set("UPDATE", "this_version", __version__)
        write(config)
    if OVERRIDE_IGNORE:
        config = configparser.ConfigParser(allow_no_value=True)
        config.read(config_path)
        try:
            config.add_section("LIST")
        except configparser.DuplicateSectionError:
            pass
        config.remove_option("LIST", "column_ignore_all")
        config.remove_option("LIST", "search_ignore_all")
        config.remove_option("LIST", "column_ignore")
        config.remove_option("LIST", "search_ignore")
        config.set("LIST", "# ligne = 0, code = 1, auteur = 2, titre = 3, level = 4, level def = 5, commentaire", "6")
        config.set("LIST", "column_ignore_all", "1,5")
        config.set("LIST", "search_ignore_all", "5,6")
        config.set("LIST", "column_ignore", "1,4,5")
        config.set("LIST", "search_ignore", "4,5,6")
        write(config)


def write(conf):
    try:
        f = open(config_path, "w")
        conf.write(f)
    except IOError:
        pass
    else:
        f.close()


if __name__ == '__main__':
    create_env()
