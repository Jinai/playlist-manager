# -*- coding: utf-8 -*-
# !python3

from distutils.version import StrictVersion

import requests
import requests.exceptions

from _version import __version__

url_version = "http://playlist-manager.olympe.in/update/version.txt"
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0'}

def available():
    return StrictVersion(latest_version()) > StrictVersion(__version__)


def latest_version():
    try:
        latest_ver = requests.get(url_version, headers=headers).text
    except requests.exceptions.RequestException:
        latest_ver = "0.1"  # Puts an old version so that it doesn't trigger the update
    return latest_ver
