# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk

from widgets import labelgif


class IconSwitch(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.icon_static = None
        self.icon_animated = None
        self.cancel = None

    def switch(self, base64, hide_delay=2000, gif=False):
        self.hide()
        if gif:
            self.icon_animated = labelgif.LabelGif(self)
            self.icon_animated.from_string(base64)
            self.icon_animated.pack()
        else:
            self.icon_static = ttk.Label(self)
            self.icon_static.icon = tk.PhotoImage(data=base64)
            self.icon_static.configure(image=self.icon_static.icon)
            self.icon_static.pack()
        if hide_delay:
            self.cancel = self.after(hide_delay, self.hide)

    def hide(self):
        if self.cancel is not None:
            self.after_cancel(self.cancel)
            self.cancel = None
        if self.icon_static is not None:
            self.icon_static.destroy()
        if self.icon_animated is not None:
            self.icon_animated.destroy()


class IconTitle(tk.Frame):
    def __init__(self, icon_base64, text, compound="left", color="black", master=None, **opts):
        tk.Frame.__init__(self, master, **opts)
        ttk.Style().configure("Icon.TLabel", foreground=color)
        self.icon = tk.PhotoImage(data=icon_base64)
        self.label = ttk.Label(self, text=text, image=self.icon, compound=compound, style="Icon.TLabel")
        self.label.pack()
