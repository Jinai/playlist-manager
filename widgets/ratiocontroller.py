# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk


class RatioController(ttk.LabelFrame):
    def __init__(self, parent, labels, playlist, values=(0, 0, 0), **opts):
        ttk.LabelFrame.__init__(self, parent, **opts)
        self.parent = parent
        self.labels = labels  # Expects a 3-tuple of strings
        self.playlist = playlist  # Expects a Playlist object
        self.values = values
        self._easy = tk.IntVar()
        self._medium = tk.IntVar()
        self._hard = tk.IntVar()
        self._amount = tk.IntVar()
        self._random = tk.BooleanVar()
        self._max = 100  # Max value that a ratio can hold
        self._def_amount = 200
        self._prev = list(values)  # Holds the previous value of each ratio
        self._setup_widgets()

    def _setup_widgets(self):
        main_top = tk.Frame(self)
        main_top.pack(fill='both', expand=True)
        frame_label = tk.Frame(main_top)
        frame_label.pack(side='left', fill='y', expand=True)
        label_easy = ttk.Label(frame_label, text=self.labels[0], justify='left')
        label_easy.pack(fill='both', expand=True)
        label_medium = ttk.Label(frame_label, text=self.labels[1])
        label_medium.pack(fill='both', expand=True, pady=5)
        label_hard = ttk.Label(frame_label, text=self.labels[2], justify='left')
        label_hard.pack(fill='both', expand=True)

        frame_scale = tk.Frame(main_top)
        frame_scale.pack(fill='both', expand=True, side='left', padx=5)
        self.scale_easy = ttk.Scale(frame_scale, from_=0, to=100, variable=self._easy, orient='horizontal',
                                    command=lambda _: self.cmd(0), length=150)
        self.scale_easy.pack(fill='both', expand=True)
        self.scale_medium = ttk.Scale(frame_scale, from_=0, to=100, variable=self._medium, orient='horizontal',
                                      command=lambda _: self.cmd(1), length=150)
        self.scale_medium.pack(fill='both', expand=True)
        self.scale_hard = ttk.Scale(frame_scale, from_=0, to=100, variable=self._hard, orient='horizontal',
                                    command=lambda _: self.cmd(2), length=150)
        self.scale_hard.pack(fill='both', expand=True)

        frame_ratio = tk.Frame(main_top)
        frame_ratio.pack(fill='both', expand=True, side='left')
        ratio_easy = ttk.Label(frame_ratio, width=3, textvariable=self._easy, justify='right')
        ratio_easy.pack(fill='both', expand=True)
        ratio_medium = ttk.Label(frame_ratio, width=3, textvariable=self._medium, justify='right')
        ratio_medium.pack(fill='both', expand=True, pady=5)
        ratio_hard = ttk.Label(frame_ratio, width=3, textvariable=self._hard, justify='right')
        ratio_hard.pack(fill='both', expand=True)

        frame_percent = tk.Frame(main_top)
        frame_percent.pack(fill='both', expand=True, side='left')
        percent_easy = ttk.Label(frame_percent, width=3, text="%", justify='right')
        percent_easy.pack(fill='both', expand=True)
        percent_medium = ttk.Label(frame_percent, width=3, text="%", justify='right')
        percent_medium.pack(fill='both', expand=True, pady=5)
        percent_hard = ttk.Label(frame_percent, width=3, text="%", justify='right')
        percent_hard.pack(fill='both', expand=True)

        main_bottom = tk.Frame(self)
        main_bottom.pack(fill='x')
        bot_settings1 = tk.Frame(main_bottom)
        bot_settings1.pack(fill='x')
        self.cb_random = ttk.Checkbutton(bot_settings1, text="Random", variable=self._random, command=self.state)
        self.cb_random.pack(side='left')
        frame_config = tk.Frame(bot_settings1)
        frame_config.pack(side='right')
        self.config0 = ttk.Button(frame_config, text='@', width=2, command=lambda: self.config(0))
        self.config0.pack(side='left')
        config1 = ttk.Button(frame_config, text="1", width=2, command=lambda: self.config(1))
        config1.pack(side='left')
        config2 = ttk.Button(frame_config, text="2", width=2, command=lambda: self.config(2))
        config2.pack(side='left')
        config3 = ttk.Button(frame_config, text="3", width=2, command=lambda: self.config(3))
        config3.pack(side='left')
        config4 = ttk.Button(frame_config, text="4", width=2, command=lambda: self.config(4))
        config4.pack(side='left')

        bot_settings2 = tk.Frame(main_bottom)
        bot_settings2.pack(fill='x')
        frame_amount = tk.Frame(bot_settings2)
        frame_amount.pack(side='left')
        label_amount = ttk.Label(bot_settings2, text="Total : ")
        label_amount.pack(side='left')
        self.entry_amount = ttk.Entry(bot_settings2, width=5, textvariable=self._amount)
        self.entry_amount.pack(side='left')
        label_amount2 = ttk.Label(bot_settings2, text="maps")
        label_amount2.pack(side='left')
        self.button_reset = ttk.Button(bot_settings2, text="Reset", command=self.reset, width=5)
        self.button_reset.pack(side='right')

        self.reset()

    def cmd(self, scale_id):
        v = [self._easy, self._medium, self._hard]
        prev = self._prev[scale_id]
        cur = int(v[scale_id].get())

        if cur > prev:
            adjust = sum(self.get()) > self._max
            if adjust:
                excess = cur - prev
                choice = self.get()[:scale_id] + self.get()[scale_id + 1:]
                lo, hi = min(choice), max(choice)
                lo_index, hi_index = self.get().index(lo), self.get().index(hi)
                if lo - excess >= 0:
                    v[lo_index].set(lo - excess)
                    self._prev[lo_index] = lo - excess
                elif hi - excess >= 0:
                    v[hi_index].set(hi - excess)
                    self._prev[hi_index] = hi - excess

        v[scale_id].set(cur)
        self._prev[scale_id] = cur

    def state(self):
        if self._random.get():
            self.scale_easy.state(['disabled'])
            self.scale_medium.state(['disabled'])
            self.scale_hard.state(['disabled'])
        else:
            self.scale_easy.state(['!disabled'])
            self.scale_medium.state(['!disabled'])
            self.scale_hard.state(['!disabled'])

    def reset(self):
        self.set(*self.values)
        self._amount.set(self._def_amount)
        self._random.set(1)
        self.state()

    def config(self, conf_id):
        if conf_id == 0:
            r = map(lambda x: round(x), self.playlist.ratios())
            self.set(*r)
        elif conf_id == 1:
            self.set(10, 70, 20)
        elif conf_id == 2:
            self.set(33, 34, 33)
        elif conf_id == 3:
            self.set(40, 40, 20)
        elif conf_id == 4:
            self.set(20, 40, 40)
        self._random.set(0)
        self.state()

    def amount(self):
        return self._amount.get()

    def random(self):
        return self._random.get()

    def get(self):
        return self._easy.get(), self._medium.get(), self._hard.get()

    def set(self, e, m, h):
        self._easy.set(e)
        self._medium.set(m)
        self._hard.set(h)


if __name__ == '__main__':
    import playlist
    import icondata

    p = playlist.Playlist()
    p.open('playlists/master_playlist.txt')
    root = tk.Tk()
    icon = tk.PhotoImage(data=icondata.icon_gear)
    title = ttk.Label(text="Paramètres", image=icon, compound="left")
    title.icon = icon
    rc = RatioController(root, ('Easy :', 'Medium :', 'Hard :'), p, labelwidget=title)
    rc.pack(fill='both', expand=True)
    rc.playlist = p
    root.mainloop()
