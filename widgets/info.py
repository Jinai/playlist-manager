# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk


class Info(ttk.LabelFrame):
    def __init__(self, parent, labels, playlist, **opts):
        ttk.LabelFrame.__init__(self, parent, **opts)
        self.parent = parent
        self.labels = labels  # Expects a 4-tuple of strings
        self.playlist = playlist  # Expects a Playlist object
        self._easy = tk.IntVar()
        self._easypercent = tk.IntVar()
        self._medium = tk.IntVar()
        self._mediumpercent = tk.IntVar()
        self._hard = tk.IntVar()
        self._hardpercent = tk.IntVar()
        self._total = tk.IntVar()
        self._setup_widgets()

    def _setup_widgets(self):
        frame_easy = tk.Frame(self)
        frame_easy.pack(fill='x', expand=True)
        ttk.Label(frame_easy, text=self.labels[0], justify='left').pack(side='left')
        frame_numbers = tk.Frame(frame_easy)
        frame_numbers.pack(side='right')
        ttk.Label(frame_numbers, textvariable=self._easypercent, font="arial 7", justify='right').pack(
            side='right', fill='x', padx=(5, 0), anchor='se')
        ttk.Label(frame_numbers, textvariable=self._easy, foreground='green', justify='right').pack()

        frame_medium = tk.Frame(self)
        frame_medium.pack(fill='x', expand=True)
        ttk.Label(frame_medium, text=self.labels[1], justify='left').pack(side='left')
        frame_numbers = tk.Frame(frame_medium)
        frame_numbers.pack(side='right')
        ttk.Label(frame_numbers, textvariable=self._mediumpercent, font="arial 7", justify='right').pack(
            side='right', fill='x', padx=(5, 0), anchor='se')
        ttk.Label(frame_numbers, textvariable=self._medium, foreground='green', justify='right').pack(side='right')

        frame_hard = tk.Frame(self)
        frame_hard.pack(fill='x', expand=True)
        ttk.Label(frame_hard, text=self.labels[2], justify='left').pack(side='left')
        frame_numbers = tk.Frame(frame_hard)
        frame_numbers.pack(side='right')
        ttk.Label(frame_numbers, textvariable=self._hardpercent, font="arial 7", justify='right').pack(
            side='right', fill='x', padx=(5, 0), anchor='se')
        ttk.Label(frame_numbers, textvariable=self._hard, foreground='green', justify='right').pack(side='right')

        ttk.Separator(self, orient="horizontal").pack(fill='both', expand=True, anchor='s', padx=10, pady=(10, 5))

        frame_total = tk.Frame(self)
        frame_total.pack(fill='x', expand=True)
        ttk.Label(frame_total, text=self.labels[3], justify='left').pack(side='left')
        ttk.Label(frame_total, textvariable=self._total, foreground='blue', justify='right').pack(side='right',
                                                                                                  padx=(0, 40))

    def update(self):
        e, m, h = self.playlist.all_count()
        self._easy.set(e)
        self._medium.set(m)
        self._hard.set(h)
        self._total.set(e + m + h)
        e, m, h = self.playlist.ratios()
        self._easypercent.set("{:5.2f}%".format(e))
        self._mediumpercent.set("{:5.2f}%".format(m))
        self._hardpercent.set("{:5.2f}%".format(h))


if __name__ == '__main__':
    import playlist
    import icondata
    from widgets import iconutils

    p = playlist.Playlist()
    p.open('playlists/master_playlist.txt')
    root = tk.Tk()
    title = iconutils.IconTitle(icondata.icon_info4, "Info", padx=2)
    info = Info(root, ('Easy maps :', 'Medium maps :', 'Hard maps :', 'Total :'), p, labelwidget=title)
    info.pack(fill='both', expand=1)
    info.update()
    root.mainloop()
