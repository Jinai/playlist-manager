# -*- coding: utf-8 -*-
# !python3

import sys
import tkinter as tk
import tkinter.ttk as ttk

import playlist
import icondata


def str2bool(s):
    return s.lower() in ('y', 'yes', 't', 'true', '1', 'on')


class Form(ttk.LabelFrame):
    def __init__(self, parent, labels, playlist, logger, entryWidth=15, refresh_cmd=lambda: None, **opts):
        ttk.LabelFrame.__init__(self, parent, **opts)
        self.parent = parent
        self.labels = labels  # Expects a 4-tuple of strings
        self.playlist = playlist  # Expects a Playlist object
        self.logger = logger  # Expects a Logger object
        self.entryWidth = entryWidth
        self.refresh_cmd = refresh_cmd  # callback when adding a map (refreshes trees / info)
        self._code = tk.StringVar()
        self._titre = tk.StringVar()
        self._auteur = tk.StringVar()
        self._comment = tk.StringVar()
        self._level = tk.StringVar()
        self._definite = tk.StringVar()
        self._setup_widgets()

    def _setup_widgets(self):
        frame_code = tk.Frame(self)
        frame_code.pack(fill='x')
        self.label_code = ttk.Label(frame_code, text=self.labels[0], justify='left')
        self.label_code.pack(side='left')
        self.entry_code = ttk.Entry(frame_code, textvariable=self._code, width=self.entryWidth)
        self.entry_code.pack(fill='x', side='right')

        frame_titre = tk.Frame(self)
        frame_titre.pack(fill='x', pady=1)
        self.label_titre = ttk.Label(frame_titre, text=self.labels[1], justify='left')
        self.label_titre.pack(side='left')
        self.entry_titre = ttk.Entry(frame_titre, textvariable=self._titre, width=self.entryWidth)
        self.entry_titre.pack(fill='x', side='right')

        frame_auteur = tk.Frame(self)
        frame_auteur.pack(fill='x')
        self.label_auteur = ttk.Label(frame_auteur, text=self.labels[2], justify='left')
        self.label_auteur.pack(side='left')
        self.entry_auteur = ttk.Entry(frame_auteur, textvariable=self._auteur, width=self.entryWidth)
        self.entry_auteur.pack(fill='x', side='right')

        frame_comment = tk.Frame(self)
        frame_comment.pack(fill='x', pady=(1, 0))
        self.label_comment = ttk.Label(frame_comment, text=self.labels[3], justify='left')
        self.label_comment.pack(side='left')
        self.entry_comment = ttk.Entry(frame_comment, textvariable=self._comment, width=self.entryWidth)
        self.entry_comment.pack(fill='x', side='right')

        frame_level = tk.Frame(self)
        frame_level.pack(fill='x')
        self.rb_easy = ttk.Radiobutton(frame_level, text='Easy', variable=self._level, value='easy')
        self.rb_medium = ttk.Radiobutton(frame_level, text='Medium', variable=self._level, value='medium')
        self.rb_hard = ttk.Radiobutton(frame_level, text='Hard', variable=self._level, value='hard')
        self.rb_easy.grid(row=0, column=0)
        self.rb_medium.grid(row=0, column=1)
        self.rb_hard.grid(row=0, column=2)
        frame_level.grid_columnconfigure((0, 1, 2), weight=1)

        frame_definite = tk.Frame(self)
        frame_definite.pack(fill='x')
        self.rb_definite = ttk.Radiobutton(frame_definite, text="Level sûr", variable=self._definite, value=1)
        self.rb_not_definite = ttk.Radiobutton(frame_definite, text="Pas sûr", variable=self._definite, value=0)
        self.rb_definite.grid(row=0, column=0)
        self.rb_not_definite.grid(row=0, column=1)
        frame_definite.grid_columnconfigure((0, 1), weight=1)

        frame_buttons = tk.Frame(self)
        frame_buttons.pack(fill='x')
        icon = tk.PhotoImage(data=icondata.icon_add)
        button_add = ttk.Button(frame_buttons, text="Ajouter", command=self.add, image=icon, compound="left")
        button_add.icon = icon
        button_add.pack(side='left')
        icon = tk.PhotoImage(data=icondata.icon_eraser)
        button_clear = ttk.Button(frame_buttons, text="Effacer", command=self.clear, image=icon, compound="left")
        button_clear.icon = icon
        button_clear.pack(side='right')
        self.clear()

        # Binds Enter to 'Add'
        self.entry_code.bind('<Return>', lambda _: button_add.invoke())
        self.entry_titre.bind('<Return>', lambda _: button_add.invoke())
        self.entry_auteur.bind('<Return>', lambda _: button_add.invoke())
        self.entry_comment.bind('<Return>', lambda _: button_add.invoke())
        self.rb_definite.bind('<Return>', lambda _: button_add.invoke())
        self.rb_not_definite.bind('<Return>', lambda _: button_add.invoke())

    def add(self):
        try:
            m = playlist.Map(*self.get())
        except ValueError as e:
            self.log(str(e), tag='warning')
        except:
            self.log(sys.exc_info()[1], tag='failure')
        else:
            if self.playlist.add(m):
                self.log("Nouvelle map : {}".format(str(m)))
            else:
                self.log("Map doublon : {} ({})".format(m.code, self.playlist.duplicates[m]))
            self.refresh_cmd()

    def clear(self):
        self.set(code='', titre='', auteur='', level='medium', comment='', definite_lvl=True)

    def get(self):
        return (self._code.get(), self._titre.get(), self._auteur.get(), self._level.get(), self._comment.get(),
                str2bool(self._definite.get()))

    def set(self, **kwargs):
        kw = {"code": self._code,
              "titre": self._titre,
              "auteur": self._auteur,
              "level": self._level,
              "comment": self._comment,
              "definite_lvl": self._definite}
        for arg in kwargs:
            kw[arg].set(kwargs[arg])

    def log(self, event, tag=None, timestamp=True):
        self.logger.insert(event, tag, timestamp)


if __name__ == '__main__':
    import logger

    root = tk.Tk()
    p = playlist.Playlist()
    p.open("playlists/master_playlist.txt")
    l = logger.Logger(root)
    form = Form(root, ('Code :', 'Titre :', 'Auteur :', 'Commentaire :'), p, l)
    form.pack()
    l.pack()
    root.mainloop()
