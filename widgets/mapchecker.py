# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as mbox
import tkinter.filedialog as fdialog

import pebble
import requests
import requests.packages
import requests.exceptions

import playlist
import icondata
from widgets import iconutils


URL_PLAYLIST = "http://playlist-manager.olympe.in/playlists/master_playlist.txt"
HEADERS = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0'}
TIMEOUT = 10
VERIFY = True


class MapChecker(ttk.LabelFrame):
    def __init__(self, master, playlist, logger, form, refresh_cmd=lambda: None, line_break="\n", **opts):
        ttk.LabelFrame.__init__(self, master, **opts)
        self.master = master
        self.playlist = playlist  # Expects a Playlist object
        self.logger = logger  # Expects a Logger object
        self.form = form  # Expects a Form object
        self.refresh_cmd = refresh_cmd  # callback when deleting duplicates (refreshes trees / info)
        self.line_break = line_break  # Splits downloaded playlist at newlines using this
        self._checkvar = tk.StringVar()
        self._master_playlist = None
        self._errors = []
        self._setup_widgets()

    def _setup_widgets(self):
        self.frame_check = tk.Frame(self)
        self.frame_check.pack(fill='x', padx=1)
        label_check = ttk.Label(self.frame_check, text="Code : ")
        label_check.pack(side='left')
        entry_check = ttk.Entry(self.frame_check, textvariable=self._checkvar)
        entry_check.pack(side='left', fill='x', expand=True)
        entry_check.bind('<Return>', lambda _: self.button_check.invoke())
        self.icon = iconutils.IconSwitch(self.frame_check)
        self.icon.switch(icondata.icon_empty)
        self.icon.pack()
        frame_buttons = tk.Frame(self)
        frame_buttons.pack(fill="x", expand=True)
        self.button_check = ttk.Button(frame_buttons, text="   Vérifier     ",
                                       command=lambda: self.run(self.compare_input))
        self.button_compare = ttk.Button(frame_buttons, text=" Comparer ",
                                         command=lambda: self.run(self.compare_playlist))
        self.button_download = ttk.Button(frame_buttons, text="Télécharger",
                                          command=lambda: self.run(self.download_playlist, override=True))
        self.button_refresh = ttk.Button(frame_buttons, text="MÀJ cache",
                                        command=lambda: self.run(self.refresh_cache, override=True))
        self.button_check.grid(row=0, column=0, sticky='ew')
        self.button_compare.grid(row=0, column=1, sticky='ew')
        self.button_download.grid(row=1, column=0, sticky='ew')
        self.button_refresh.grid(row=1, column=1, sticky='ew')
        frame_buttons.grid_rowconfigure((0, 1), weight=1)
        frame_buttons.grid_columnconfigure((0, 1), weight=1)

        # Icons
        self.button_check.icon = tk.PhotoImage(data=icondata.icon_verification)
        self.button_check.configure(image=self.button_check.icon, compound="left")
        self.button_compare.icon = tk.PhotoImage(data=icondata.icon_comparison)
        self.button_compare.configure(image=self.button_compare.icon, compound="left")
        self.button_download.icon = tk.PhotoImage(data=icondata.icon_download)
        self.button_download.configure(image=self.button_download.icon, compound="left")
        self.button_refresh.icon = tk.PhotoImage(data=icondata.icon_refresh)
        self.button_refresh.configure(image=self.button_refresh.icon, compound="left")

    def get_data(self):
        try:
            data = requests.get(URL_PLAYLIST, headers=HEADERS, timeout=TIMEOUT, verify=VERIFY)
        except requests.exceptions.ConnectionError as e:
            self.log("Connection Error\n" + str(e), tag='failure')
        except requests.exceptions.Timeout as e:
            self.log("Timeout\n" + str(e), tag='failure')
        else:
            p = playlist.Playlist()
            p.from_string(data.text)
            self._master_playlist = p

    def run(self, callback, override=False):
        self.icon.switch(icondata.icon_loading, hide_delay=0, gif=True)
        if override:
            pebble.thread.concurrent(target=self.get_data, callback=callback)
        elif self._master_playlist is None:
            pebble.thread.concurrent(target=self.get_data, callback=callback)
        else:
            callback()

    def compare_input(self, task=None):
        self.icon.hide()
        key = self._checkvar.get()
        if playlist.Map.is_valid(key) and self._master_playlist:
            found = False
            for i, m in enumerate(self._master_playlist):
                if key == m.code:
                    found = True
                    self.log("Doublon : {} [Ligne {}]".format(str(m), str(i + 1)))
                    break
            if not found:
                self.icon.switch(icondata.icon_valid)
                self.log("Unique : {}".format(key))
                self.form.set(code=key)
            else:
                self.icon.switch(icondata.icon_invalid)
        else:
            self.icon.switch(icondata.icon_invalid)

    def compare_playlist(self, task=None):
        self.icon.hide()
        if self._master_playlist:
            duplicates = []
            new = []
            for m in self.playlist:
                if m in self._master_playlist:
                    duplicates.append(m)
                else:
                    new.append(m)
            self.log("{} maps uniques, {} doublons".format(len(new), len(duplicates)), tag='success')
            if len(duplicates) > 0:
                msg = ("Il y a {} doublons dans votre playlist par rapport à la playlist centrale."
                       "\nSouhaitez-vous les supprimer ?".format(len(duplicates)))
                if mbox.askyesno("Doublons", msg):
                    for m in duplicates:
                        self.playlist.remove(m)
                    self.log("{} doublons supprimés".format(len(duplicates)))
                    self.refresh_cmd()

    def download_playlist(self, task=None):
        self.icon.hide()
        filename = fdialog.asksaveasfilename(defaultextension='.txt', initialfile='master_playlist')
        if filename:
            try:
                f = open(filename, 'w', encoding='utf-8')
                f.write(str(self._master_playlist))
            except IOError as e:
                self.log(str(e), tag='failure')
            else:
                self.log("Playlist centrale : {}".format(filename))

    def refresh_cache(self, task=None):
        self.icon.hide()
        self.log("Cache du map checker mis à jour")

    def log(self, event, tag=None, timestamp=True):
        self.logger.insert(event, tag=tag, timestamp=timestamp)


if __name__ == '__main__':
    import logger
    import form

    root = tk.Tk()
    p = playlist.Playlist()
    p.open('playlists/master_playlist.txt')
    l = logger.Logger(root)
    f = form.Form(root, ('Code :', 'Titre :', 'Auteur :', 'Commentaire :'), p, l)
    title = iconutils.IconTitle(icondata.icon_file_valid, "Map checker", padx=2)
    mc = MapChecker(root, playlist=p, logger=l, form=f, labelwidget=title)
    mc.pack(fill='both', expand=True)
    f.pack()
    l.pack()
    root.mainloop()
