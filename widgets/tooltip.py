# -*- coding: utf-8 -*-
# !python3

import tkinter as tk


def _children(widget):
    children = widget.winfo_children()
    for child in children:
        if child.winfo_children():
            children.extend(child.winfo_children())
    return children


class ToolTipBase:
    def __init__(self, widget, delay=500):
        self.widget = widget
        self.delay = delay
        self.tipwindow = None
        self.id = None
        self.visible = True
        widget.bind("<Enter>", self.enter, add="+")
        widget.bind("<Leave>", self.leave, add="+")
        widget.bind("<Button-1>", self.leave, add="+")

    def enter(self, event):
        self.schedule(event)

    def leave(self, event):
        self.unschedule()
        self.hide_tip()

    def schedule(self, event):
        self.unschedule()
        self.id = self.widget.after(self.delay, lambda: self.show_tip(event))

    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.widget.after_cancel(id)

    def hide_tip(self):
        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()

    def toggle(self):
        self.visible = not self.visible

    def show_tip(self, *args):
        pass

    def show_content(self, *args):
        pass


class StaticToolTip(ToolTipBase):
    def __init__(self, widget, text, delay=500, offset=(10, 0), propagate=False):
        ToolTipBase.__init__(self, widget, delay)
        self.text = text
        self.offset = offset  # widget offset
        if propagate:
            for widget in _children(self.widget):
                widget.bind("<Enter>", self.enter, add="+")
                widget.bind("<Leave>", self.leave, add="+")
                widget.bind("<Button-1>", self.leave, add="+")

    def show_tip(self, event):
        if self.tipwindow:
            return
        if self.visible:
            x = self.widget.winfo_rootx() + self.widget.winfo_width() + self.offset[0]
            y = self.widget.winfo_rooty() + self.offset[1]
            self.tipwindow = tw = tk.Toplevel(self.widget)
            tw.wm_overrideredirect(1)
            tw.wm_geometry("+{}+{}".format(x, y))
            self.show_content()

    def show_content(self):
        label = tk.Label(self.tipwindow, text=self.text, justify='left', background="#ffffe0", relief='solid',
                         borderwidth=1)
        label.pack()


class DynamicToolTip(ToolTipBase):
    def __init__(self, widget, text, delay=500, offset=(15, 10), propagate=False):
        ToolTipBase.__init__(self, widget, delay)
        self.text = text
        self.offset = offset  # cursor offset
        widget.bind('<Motion>', self.move, add="+")
        if propagate:
            for widget in _children(self.widget):
                widget.bind("<Enter>", self.enter, add="+")
                widget.bind("<Leave>", self.leave, add="+")
                widget.bind("<Button-1>", self.leave, add="+")
                widget.bind('<Motion>', self.move, add="+")

    def show_tip(self, event=None):
        if self.visible:
            x = event.x_root + self.offset[0]
            y = event.y_root + self.offset[1]
            self.tipwindow = tw = tk.Toplevel(self.widget)
            tw.wm_overrideredirect(1)
            tw.wm_geometry("+{}+{}".format(x, y))
            self.show_content()

    def show_content(self):
        label = tk.Label(self.tipwindow, text=self.text, justify='left', background="#ffffe0", relief='solid',
                         borderwidth=1)
        label.pack()

    def move(self, event):
        if self.tipwindow:
            x = event.x_root + 15
            y = event.y_root + 10
            tw = self.tipwindow
            tw.wm_geometry("+{}+{}".format(x, y))
        else:
            self.schedule(event)


if __name__ == '__main__':
    root = tk.Tk()
    frame = tk.Frame(root)
    frame.pack(fill='both', expand=True, padx=20, pady=20)
    b = tk.Button(frame, text="Static Tooltip", command=lambda: print('widget1 pressed'))
    b.pack(fill='x', expand=True, pady=(0, 20))
    b2 = tk.Button(frame, text="Dynamic Tooltip", command=lambda: print('widget2 pressed'))
    b2.pack(side='bottom', fill='x', expand=True)

    tip1 = StaticToolTip(b, "Hello World !")
    tip2 = DynamicToolTip(b2, "Hello world !")

    root.mainloop()
