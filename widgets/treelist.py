# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.font as tkfont

import pyperclip

import playlist
import icondata


def str2bool(s):
    return s.lower() in ('y', 'yes', 't', 'true', '1', 'on')


class Treelist(tk.Frame):
    def __init__(self, parent, headers, height=15, alt_color=True, **opts):
        tk.Frame.__init__(self, parent, **opts)
        self.parent = parent
        self.headers = ["#"]
        self.headers.extend(headers)
        self.height = height
        self.alt_color = alt_color  # Distinguishes rows if True
        self._search_key = tk.StringVar()
        self._data = []
        self._item_count = 0
        self._setup_widgets()

    def _setup_widgets(self):
        frame_tree = tk.Frame(self)
        frame_tree.pack(fill='both', expand=True)
        vsb = ttk.Scrollbar(frame_tree, orient="vertical")
        vsb.pack(side='right', fill='y')
        hsb = ttk.Scrollbar(frame_tree, orient="horizontal")
        hsb.pack(side='bottom', fill='x')
        self.tree = ttk.Treeview(frame_tree, columns=self.headers, displaycolumns=self.headers, show="headings",
                                 height=self.height, selectmode="extended")
        self.tree.pack(side='top', fill='both', expand=True)
        self.tree.configure(yscrollcommand=vsb.set, xscrollcommand=hsb.set)
        vsb.configure(command=self.tree.yview)
        hsb.configure(command=self.tree.xview)
        # Tags
        self.tree.tag_configure("odd_row", background="white")
        self.tree.tag_configure("even_row", background="grey97")

        self._build_tree()

    def _build_tree(self):
        for header in self.headers:
            self.tree.heading(header, text=header.title(), anchor="w", command=lambda h=header: self.sort(h, False))
            # Adjust header width with empirical values
            if header == "#":
                self.tree.column(header, width=30, stretch=False)
            elif header == "auteur":
                self.tree.column(header, width=90, stretch=False)
            elif header == "titre":
                self.tree.column(header, width=100, stretch=False)
            elif header == "level":
                self.tree.column(header, width=55, stretch=False)
            else:
                width = tkfont.Font().measure(header.title())
                self.tree.column(header, width=width, stretch=False)
        # Last column always stretcheable so that it takes up the space when resizing
        self.tree.column(self.headers[-1], stretch=True)

    def insert(self, values, update=True):
        self._item_count += 1
        self.tree.insert('', 'end', values=values)
        if update:
            self._data.append(values)

    def delete(self):
        for item in self.tree.selection():
            self._data.remove(list(self.tree.item(item)['values']))
            self.tree.delete(item)
            self._item_count -= 1

    def clear(self):
        self.tree.delete(*self.tree.get_children())
        self._item_count = 0

    def sort(self, col, descending):
        data = [(self.tree.set(child, col), child) for child in self.tree.get_children('')]
        if col == "#":  # Special case of line numbers
            data.sort(reverse=descending, key=lambda x: int(x[0]))
        else:
            data.sort(reverse=descending, key=lambda x: x[0].lower())
        for index, item in enumerate(data):
            self.tree.move(item[1], '', index)
        # Switch heading command to reverse the sort next time
        self.tree.heading(col, command=lambda col=col: self.sort(col, not descending))

    def search(self):
        key = self._search_key.get()
        self.clear()
        for values in self._data:
            for item in values:
                if key.lower() in item.lower():
                    self.insert(values, update=False)
                    break

    def select_all(self):
        self.tree.selection_set(self.tree.get_children())


class Maplist(Treelist):
    def __init__(self, parent, headers, playlist, level, logger, column_ignore=(1, 4, 5), search_ignore=(4, 5, 6),
                 refresh_cmd=lambda: None, **opts):
        Treelist.__init__(self, parent, headers, **opts)
        self.parent = parent
        self.headers = ["#"]
        self.headers.extend(headers)
        self.playlist = playlist  # Expects a Playlist object
        self.level = level  # 'easy', 'medium' or 'hard'
        self.logger = logger  # Expects a Logger object
        self.column_ignore = column_ignore  # Tells what columns to hide
        self.search_ignore = search_ignore  # Tells what columns are irrelevant when searching
        self.refresh_cmd = refresh_cmd  # callback when deleting an item (updates info)
        self._search_key = tk.StringVar()
        self._search_key.trace("w", lambda _, __, ___: self.search())
        self._search_options = {
            'code': tk.BooleanVar(),
            'auteur': tk.BooleanVar(),
            'titre': tk.BooleanVar(),
            'def_lvl': tk.BooleanVar(),
            'comment': tk.BooleanVar(),
        }
        self.init_widgets()

    def init_widgets(self):
        self.frame_options = tk.Frame(self)
        self.frame_options.pack(fill='x', padx=5)
        self.labelframe_search = ttk.LabelFrame(self.frame_options, text="Rechercher")
        self.frame_searchbox = tk.Frame(self.labelframe_search, bg='white', bd=1, relief='sunken')
        self.frame_searchbox.pack(fill='x', expand=True, padx=5, pady=(0, 6))
        self.entry_search = tk.Entry(self.frame_searchbox, textvariable=self._search_key, width=15, bd=0,
                                     highlightthickness=0)
        self.entry_search.pack(fill='x', expand=True, side='left')
        self.label_search = ttk.Label(self.frame_searchbox, background='white')
        self.label_search.pack(side='left')
        self.button_selectall = ttk.Button(self.frame_options, text="Tout sélectionner", command=self.select_all)
        self.button_delete = ttk.Button(self.frame_options, text="Supprimer", command=self.delete)
        self.button_quickpl = ttk.Button(self.frame_options, text="Quick playlist",
                                         command=lambda: self.quickpl(self.tree.selection()))
        self.ignore_columns(self.column_ignore)

        # Tags
        self.tree.tag_configure("unsure", foreground="red")
        # Bindings
        self.tree.bind('<BackSpace>', lambda _: self.delete())
        self.tree.bind('<Delete>', lambda _: self.delete())
        self.tree.bind('<Double-1>', self.on_doubleclick)
        self.tree.bind('<Control-a>', lambda _: self.select_all())
        # Grid placement
        self.labelframe_search.grid(row=0, column=0, columnspan=2, sticky="ew")
        self.button_selectall.grid(row=0, column=2, sticky="sew", padx=5, pady=(0, 7))
        self.button_delete.grid(row=0, column=3, sticky="sew", pady=(0, 7))
        self.button_quickpl.grid(row=1, column=0, columnspan=4, sticky="ew", pady=5)
        self.frame_options.grid_columnconfigure((0, 1), weight=1)
        # Icons
        self.label_search.icon = tk.PhotoImage(data=icondata.icon_search)
        self.label_search.configure(image=self.label_search.icon)
        self.button_delete.icon = tk.PhotoImage(data=icondata.icon_trashbin)
        self.button_delete.configure(image=self.button_delete.icon)
        self.button_quickpl.icon = tk.PhotoImage(data=icondata.icon_quickplaylist)
        self.button_quickpl.configure(image=self.button_quickpl.icon, compound="left")

    def insert(self, values, update=True):
        self._item_count += 1
        tags = ["even_row", "odd_row", "unsure"]
        if self.alt_color:
            if not str2bool(values[5]):
                self.tree.insert('', 'end', values=values, tags=(tags[self._item_count % 2], tags[2]))
            else:
                self.tree.insert('', 'end', values=values, tags=(tags[self._item_count % 2],))
        else:
            if not str2bool(values[5]):
                self.tree.insert('', 'end', values=values, tags=tags[2])
            else:
                self.tree.insert('', 'end', values=values)
        if update:
            self._data.append(values)

    def delete(self):
        for item in self.tree.selection():
            values = list(self.tree.item(item)['values'])
            c, t, a, l, def_lvl, com = values[1], values[3], values[2], values[4], values[5], values[6]
            m = playlist.Map(c, t, a, l, definite_level=def_lvl, comment=com)
            values[0] = str(values[0])  # Force str because tkinter converts digits to int
            self._data.remove(values)
            self.tree.delete(item)
            self.playlist.remove(m)
            self.log("Map supprimée : {}".format(str(m)))
            self._item_count -= 1
        self.refresh()  # Need a refresh for line numbers and alternate rows color
        self.refresh_cmd()

    def search(self):
        keys = [key.strip().lower() for key in self._search_key.get().split('|')]
        self.clear()
        for key in keys:
            for values in self._data:
                for index, item in enumerate(values):
                    if index not in self.search_ignore:
                        if key.lower() in item.lower():
                            if len(keys) > 1:
                                if key != "":
                                    self.insert(values, update=False)
                                    break
                            else:
                                self.insert(values, update=False)
                                break

    def on_doubleclick(self, event):
        if self.tree.identify_region(event.x, event.y) == 'cell':
            values = self.tree.item(self.tree.selection())['values']
            pyperclip.copy("/load " + values[1])
            self.log("\"/load {0}\" copied to clipboard".format(values[1]))

    def quickpl(self, selection):
        p = playlist.Playlist()
        for item in selection:
            values = tuple(self.tree.item(item)['values'])
            c, t, a, l, def_lvl, com = values[1], values[3], values[2], values[4], str2bool(values[5]), values[6]
            p.add(playlist.Map(c, t, a, l, definite_level=def_lvl, comment=com))
        pyperclip.copy(str(p))
        self.log("Quick playlist ({} maps) copied to clipboard".format(len(p)))

    def populate(self):
        maps = {
            'easy': self.playlist.easy,
            'medium': self.playlist.medium,
            'hard': self.playlist.hard,
            'all': self.playlist.original
        }
        for index, m in enumerate(maps[self.level]):
            self.insert([str(index + 1), m.code, m.auteur, m.titre, m.level.name, str(m.definite_level), m.comment])

    def refresh(self):
        self.clear()
        del self._data[:]
        self.populate()

    def ignore_columns(self, columns):
        self.tree.configure(displaycolumns=[h for i, h in enumerate(self.headers) if i not in columns])

    def ignore_search(self, columns):
        self.search_ignore = columns

    def log(self, event, tag=None, timestamp=True):
        self.logger.insert(event, tag, timestamp)


if __name__ == '__main__':
    import logger

    root = tk.Tk()
    p = playlist.Playlist()
    p.open('playlists/master_playlist.txt')
    l = logger.Logger(root)
    tree = Maplist(root, ('code', 'auteur', 'titre', 'level', 'def lvl', 'comment'), p, 'all', l, column_ignore=(1, 5),
                   search_ignore=(5, 6), refresh_cmd=lambda: print('delete callback'))
    tree.pack(fill='both', expand=True)
    tree.refresh()
    root.mainloop()
