# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.filedialog as fdialog
import datetime


class Logger(tk.Frame):
    def __init__(self, parent, width=60, height=10, font="arial 8", wrap="word", enable_buttons=False,
                 dialog=None, bg_color='white', **opts):
        tk.Frame.__init__(self, parent, **opts)
        self.parent = parent
        self.width = width
        self.height = height
        self.font = font
        self.wrap = wrap
        self.enable_buttons = enable_buttons
        self.dialog = dialog if dialog else {}
        self.bg_color = bg_color
        self.timestamps = []
        self._setup_widgets()

    def _setup_widgets(self):
        frame_scrolledtext = ttk.Frame(self)
        frame_scrolledtext.pack(fill='both', expand=True)
        self.textbox = tk.Text(frame_scrolledtext, borderwidth=1, relief='sunken', bg=self.bg_color)
        self.textbox.pack(side='left', fill='both', expand=True)
        self.scrollbar = ttk.Scrollbar(frame_scrolledtext)
        self.scrollbar.pack(side='right', fill='y')
        self.textbox.config(font=self.font, width=self.width, height=self.height, wrap=self.wrap)
        self.textbox.config(yscrollcommand=self.scrollbar.set, state='disabled')
        self.scrollbar.config(command=self.textbox.yview)
        if self.enable_buttons:
            frame_buttons = ttk.Frame(self)
            frame_buttons.pack(pady=1)
            self.button_save = ttk.Button(frame_buttons)
            self.button_save.pack(side='left')
            self.button_clear = ttk.Button(frame_buttons)
            self.button_clear.pack()
            self.button_save.config(text="Sauver", width=8, command=self.save)
            self.button_clear.config(text="Effacer", width=8, command=self.clear)
        self.textbox.tag_configure('success', foreground='green')
        self.textbox.tag_configure('warning', foreground='DarkOrange2')
        self.textbox.tag_configure('failure', foreground='red')


    def insert(self, line, tag=None, timestamp=False):
        self.timestamps.append(
            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        self.textbox.config(state='normal')
        if tag:
            line = "[{}] {}".format(tag.capitalize()[0], line)
        if timestamp:
            line = "[{}] {}".format(
                datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), line)
        if self.textbox.get('1.0', 'end') == '\n':
            if tag:
                self.textbox.insert('end', line, tag)
            else:
                self.textbox.insert('end', line)
        else:
            if tag:
                self.textbox.insert('end', '\n' + line, tag)
            else:
                self.textbox.insert('end', '\n' + line)
        self.textbox.yview('end')
        self.textbox.config(state='disabled')

    def save(self, timestamp=False):
        file_name = fdialog.asksaveasfilename(**self.dialog)
        if file_name:
            self.textbox.config(state='normal')
            with open(file_name, 'w', encoding='utf-8') as out:
                if timestamp:
                    lines = int(self.textbox.index('end-1c').split('.')[0])
                    if lines > 1:
                        for i in range(lines):
                            ix = str(i + 1)
                            out.write("[{}] {}\n".format(self.timestamps[i], self.textbox.get(ix + '.0', ix + '.end')))
                else:
                    out.write(self.textbox.get(1.0, 'end'))
            self.textbox.config(state='disabled')

    def clear(self):
        del self.timestamps[:]
        self.textbox.config(state='normal')
        self.textbox.delete(1.0, 'end')
        self.textbox.config(state='disabled')


if __name__ == '__main__':
    root = tk.Tk()
    logs = Logger(root, font=('arial', 10), width=60, height=10, wrap=tk.WORD, enable_buttons=True,
                  dialog={'defaultextension': '.log', 'initialfile': 'example'})
    logs.pack(fill='both', expand=True)
    logs.insert("x\tx²\t2^x\t√x")
    for i in range(20):
        logs.insert(
            str(i) + '\t' + str(i * i) + '\t' + str(2 ** i) + '\t' + str(
                i ** 0.5))
    root.mainloop()