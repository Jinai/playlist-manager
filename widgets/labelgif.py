# -*- coding: utf-8 -*-
# !python3

import tkinter as tk
import tkinter.ttk as ttk


class LabelGif(ttk.Label):
    def __init__(self, master, frame_delay=70):
        ttk.Label.__init__(self, master)
        self.master = master
        self.frame_delay = frame_delay  # Milliseconds
        self.path = None
        self.base64 = None
        self.frames = []
        self.current_frame = 0
        self.cancel = None

    def from_path(self, path):
        self.base64 = None
        self.path = path
        self.frame0 = tk.PhotoImage(file=path, format="gif -index 0")
        self.load_frames()
        self.play()

    def from_string(self, base64):
        self.path = None
        self.base64 = base64
        self.frame0 = tk.PhotoImage(data=base64, format="gif -index 0")
        self.load_frames()
        self.play()

    def load_frames(self):
        self.frames = [self.frame0]
        i = 1
        try:
            while True:
                if self.path:
                    self.frames.append(tk.PhotoImage(file=self.path, format="gif -index " + str(i)))
                else:
                    self.frames.append(tk.PhotoImage(data=self.base64, format="gif -index " + str(i)))
                i += 1
        except tk.TclError:
            pass  # No more frames

    def play(self):
        self.configure(image=self.frames[self.current_frame])
        self.current_frame += 1
        if self.current_frame == len(self.frames):
            self.current_frame = 0
        self.cancel = self.after(self.frame_delay, self.play)

    def stop(self):
        if self.cancel is not None:
            self.after_cancel(self.cancel)
            self.cancel = None

    def reverse(self):
        self.frames = self.frames[::-1]

if __name__ == '__main__':
    import tkinter as tk
    import icondata

    root = tk.Tk()
    gif = LabelGif(root)
    gif.from_string(icondata.icon_loading)
    gif.pack()
    root.mainloop()
